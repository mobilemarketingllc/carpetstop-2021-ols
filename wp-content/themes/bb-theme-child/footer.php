<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap"<?php FLTheme::print_schema( ' itemscope="itemscope" itemtype="https://schema.org/WPFooter"' ); ?>  role="contentinfo">
		<?php

		do_action( 'fl_footer_wrap_open' );
		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		FLTheme::footer();

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();


?>

<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script>
<script>
    jQuery(function () {
        jQuery('.calculateBtn').m2Calculator({
            measureSystem: "Imperial",            
            thirdPartyName: "The Carpet Stop Inc", 
            thirdPartyEmail: "",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
            showCutSheet: false, // if false, will not include cutsheet section in return image
            showDiagram: true,  // if false, will close the popup directly 
          /*  product: {
                type: "Carpet",
                name: "Carpet 1",
                width: "6'0\"",
                length: "150'0\"",
                horiRepeat: "3'0\"",
                vertRepeat: "3'0\"",
                horiDrop: "",
                vertDrop: ""
            },
            */
            cancel: function () {
                //when user closes the popup without calculation.
            },
            callback: function (data) {
                //json format, include user input, usage and base64image
                jQuery("#callback").html(JSON.stringify(data));   
                jQuery("#usageText").val(data.usage);    
                jQuery("#image").attr("src", data.img);
                window.location.href = "https://carpetstop.com//thank-you/";
            }
        });
    });
 </script>
</body>
</html>
