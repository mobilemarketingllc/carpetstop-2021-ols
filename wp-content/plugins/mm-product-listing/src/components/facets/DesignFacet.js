import React from "react";

export default function DesignFacet({ handleFilterClick, productDesign }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productDesign = sortObject(productDesign);

  return (
    <div class="facet-wrap facet-display">
      <strong>Style</strong>
      <div className="facetwp-facet">
        {Object.keys(productDesign).map((design, i) => {
          if (design && productDesign[design] > 0) {
            return (
              <div>
                <span
                  id={`design-filter-${i}`}
                  key={i}
                  data-value={`${design.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("design", e.target.dataset.value)
                  }>
                  {" "}
                  {design} {` (${productDesign[design]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
